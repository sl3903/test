"""
Source Code for Homework 3 of ECBM E4040, Fall 2016, Columbia University

Instructor: Prof. Zoran Kostic

This code is based on
[1] http://deeplearning.net/tutorial/logreg.html
[2] http://deeplearning.net/tutorial/mlp.html
[3] http://deeplearning.net/tutorial/lenet.html
"""
import numpy

import theano
import theano.tensor as T
from theano.tensor.signal import pool
import scipy.io
import scipy.linalg.blas
from hw3_utils import shared_dataset, load_data
from hw3_nn import LogisticRegression, HiddenLayer, LeNetConvPoolLayer, train_nn
from collections import OrderedDict
import os
from os import walk
import numpy as np
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
from PIL import Image


#Problem 1
#Implement the convolutional neural network architecture depicted in HW3 problem 1
#Reference code can be found in http://deeplearning.net/tutorial/code/convolutional_mlp.py
def test_lenet(learning_rate=0.01, n_epochs=1000, nkerns=[32, 64],
               batch_size=200, verbose=False,
               translation=False,rotate=False,flip=False,noise=False):
    
    rng = numpy.random.RandomState(23455)
    
    if translation==True:
       train_set, valid_set, test_set = load_data(ds_rate=5, theano_shared=False)
       train_set[1] = np.tile(train_set[1], 5)
       train_set_x_u = translate_image(train_set[0], [ 0,-1])
       train_set_x_d = translate_image(train_set[0], [ 0, 1])
       train_set_x_r = translate_image(train_set[0], [ 1, 0])
       train_set_x_l = translate_image(train_set[0], [-1, 0])
       train_set[0] = np.vstack((train_set[0],
                              train_set_x_u,
                              train_set_x_d,
                              train_set_x_r,
                              train_set_x_l))
       test_set_x, test_set_y = shared_dataset(test_set)
       valid_set_x, valid_set_y = shared_dataset(valid_set)
       train_set_x, train_set_y = shared_dataset(train_set)
       plot_top_16(translate_image(train_set[0],[0,-1]),32,'translate_image')
       
    if rotate==True:
       train_set, valid_set, test_set = load_data(ds_rate=5, theano_shared=False)
       train_set[1] = np.tile(train_set[1], 5)
       train_set_x_5 = rotate_image(train_set[0], degree=5)
       train_set_x_10 = rotate_image(train_set[0], degree=10)
       train_set_x_n5 = rotate_image(train_set[0], degree=-5)
       train_set_x_n10 = rotate_image(train_set[0], degree=-10)
       train_set[0] = np.vstack((train_set[0],
                              train_set_x_5,
                              train_set_x_10,
                              train_set_x_n5,
                              train_set_x_n10))
       test_set_x, test_set_y = shared_dataset(test_set)
       valid_set_x, valid_set_y = shared_dataset(valid_set)
       train_set_x, train_set_y = shared_dataset(train_set)
       plot_top_16(rotate_image(train_set[0],10),32,'rotate_image')
       
    if flip==True:
       train_set, valid_set, test_set = load_data(ds_rate=2, theano_shared=False)
       train_set[1] = np.tile(train_set[1], 2)
       train_set[0] = np.vstack((train_set[0],flip_image(train_set[0])))
       test_set_x, test_set_y = shared_dataset(test_set)
       valid_set_x, valid_set_y = shared_dataset(valid_set)
       train_set_x, train_set_y = shared_dataset(train_set)
       plot_top_16(flip_image(train_set[0]),32,'flip_image')
       
    if noise==True:
       train_set, valid_set, test_set = load_data(ds_rate=2, theano_shared=False)
       train_set_n=noise_injection(train_set[0])
       train_set[0] = np.vstack((train_set[0],train_set_n))
       train_set[1] = np.tile(train_set[1], 2)
       test_set_x, test_set_y = shared_dataset(test_set)
       valid_set_x, valid_set_y = shared_dataset(valid_set)
       train_set_x, train_set_y = shared_dataset(train_set)
       plot_top_16(noise_injection(train_set[0]),32,'noise_injection_image')
       
       
       
       
    else:    
       datasets = load_data()
       train_set_x, train_set_y = datasets[0]
       valid_set_x, valid_set_y = datasets[1]
       test_set_x, test_set_y = datasets[2]

    # compute number of minibatches for training, validation and testing
    n_train_batches = train_set_x.get_value(borrow=True).shape[0]
    n_valid_batches = valid_set_x.get_value(borrow=True).shape[0]
    n_test_batches = test_set_x.get_value(borrow=True).shape[0]
    n_train_batches //= batch_size
    n_valid_batches //= batch_size
    n_test_batches //= batch_size
    
    
    

    # allocate symbolic variables for the data
    index = T.lscalar()  # index to a [mini]batch

    x = T.matrix('x')   # the data is presented as rasterized images
    y = T.ivector('y')  # the labels are presented as 1D vector of
                        # [int] labels

    ######################
    # BUILD ACTUAL MODEL #
    ######################
    print('... building the model')

    # Reshape matrix of rasterized images of shape (batch_size, 3 * 32 * 32)
    # to a 4D tensor, compatible with our LeNetConvPoolLayer
    layer0_input = x.reshape((batch_size, 3, 32, 32))

    # Construct the first convolutional pooling layer:
    # filtering reduces the image size to (32-5+1 , 32-5+1) = (28, 28)
    # maxpooling reduces this further to (28/3, 28/3) = (9, 9)
    # 4D output tensor is thus of shape (batch_size, nkerns[0], 9, 9)
    # :type filter_shape: tuple or list of length 4
    # :param filter_shape: (number of filters, num input feature maps,
    #                              filter height, filter width)
    #
    #:type image_shape: tuple or list of length 4
    #:param image_shape: (batch size, num input feature maps,
    #   image height, image width)
    layer0 = LeNetConvPoolLayer(
        rng,
        input=layer0_input,
        image_shape=(batch_size, 3, 32, 32),
        filter_shape=(nkerns[0], 3, 3, 3),
        poolsize=(2, 2)
    )

    # Construct the second convolutional pooling layer
    # filtering reduces the image size to (9-7+1, 9-7+1) = (3, 3)
    # maxpooling reduces this further to (3/2, 3/2) = (1, 1)
    # 4D output tensor is thus of shape (batch_size, nkerns[1], 1, 1)
    layer1 = LeNetConvPoolLayer(
        rng,
        input=layer0.output,
        image_shape=(batch_size, nkerns[0], 15, 15),
        filter_shape=(nkerns[1], nkerns[0], 3, 3),
        poolsize=(2, 2)
    )
    
    

    # the HiddenLayer being fully-connected, it operates on 2D matrices of
    # shape (batch_size, num_pixels) (i.e matrix of rasterized images).
    # This will generate a matrix of shape (batch_size, nkerns[1] * 2 * 2),
    # or (500, 50 * 4 * 4) = (500, 800) with the default values.
    layer2_input = layer1.output.flatten(2)

    # construct a fully-connected sigmoidal layer
    layer2 = HiddenLayer(
        rng,
        input=layer2_input,
        n_in=nkerns[1] * 6 * 6,
        n_out=4096,
        activation=T.tanh
    )
    
    layer3 = HiddenLayer(
        rng,
        input=layer2.output,
        n_in=4096,
        n_out=512,
        activation=T.tanh
    )

    # classify the values of the fully-connected sigmoidal layer
    layer4 = LogisticRegression(input=layer3.output, n_in=512, n_out=10)

    # the cost we minimize during training is the NLL of the model
    cost = layer4.negative_log_likelihood(y)

    # create a function to compute the mistakes that are made by the model
    test_model = theano.function(
        [index],
        layer4.errors(y),
        givens={
            x: test_set_x[index * batch_size: (index + 1) * batch_size],
            y: test_set_y[index * batch_size: (index + 1) * batch_size]
        }
    )

    validate_model = theano.function(
        [index],
        layer4.errors(y),
        givens={
            x: valid_set_x[index * batch_size: (index + 1) * batch_size],
            y: valid_set_y[index * batch_size: (index + 1) * batch_size]
        }
    )

    # create a list of all model parameters to be fit by gradient descent
    params =layer4.params+ layer3.params + layer2.params + layer1.params + layer0.params

    # create a list of gradients for all model parameters
    grads = T.grad(cost, params)

    # train_model is a function that updates the model parameters by
    # SGD Since this model has many parameters, it would be tedious to
    # manually create an update rule for each model parameter. We thus
    # create the updates list by automatically looping over all
    # (params[i], grads[i]) pairs.
    updates = [
        (param_i, param_i - learning_rate * grad_i)
        for param_i, grad_i in zip(params, grads)
    ]

    train_model = theano.function(
        [index],
        cost,
        updates=updates,
        givens={
            x: train_set_x[index * batch_size: (index + 1) * batch_size],
            y: train_set_y[index * batch_size: (index + 1) * batch_size]
        }
    )

    ###############
    # TRAIN MODEL #
    ###############
    print('... training')

    train_nn(train_model, validate_model, test_model,
             n_train_batches, n_valid_batches, n_test_batches,
             n_epochs, verbose)


#Problem 2.1
#Write a function to add translations
def translate_image(im, trans):
#Implement a convolutional neural network with the translation method for augmentation
 # Could be simpler

    # For format before fix replace with:
    # x = im.reshape(-1,32,32,3)
    x = im.reshape(-1,3,32,32).transpose(0,2,3,1)
    if trans[0] != 0:
        yy = np.roll(x, trans[0], axis=1)
        if trans[0] > 0:
            yy[:, (trans[0]-1), :, :] = 0
        else:
            yy[:, trans[0], :, :] = 0
    elif trans[1] != 0:
        yy = np.roll(x, trans[1], axis=2)
        if trans[1] > 0:
            yy[:, :, (trans[1]-1), :] = 0
        else:
            yy[:, :, trans[1], :] = 0

    # For format before fix remove transpose transformation
    # x = im.reshape(-1,32,32,3)
    return yy.transpose(0,3,1,2).reshape(-1, 3*32*32)
    
def test_lenet_translation():
    test_lenet(learning_rate=0.1, n_epochs=1000, nkerns=[32, 64],
               batch_size=200, verbose=False,translation=True)


#Problem 2.2
#Write a function to add roatations
def rotate_image(data,degree=10):
#Implement a convolutional neural network with the rotation method for augmentation
    for i in range(data.shape[0]):
       image=data[i]
       image=MatrixToImage(image)
       image=image.rotate(degree)
       image=ImageToMatrix(image)
       data[i]=image
    return data
    
def test_lenet_rotation():
    test_lenet(learning_rate=0.1, n_epochs=1000, nkerns=[32, 64],
               batch_size=200, verbose=False,rotate=True)

#Problem 2.3
#Write a function to flip images
def flip_image(data):
#Implement a convolutional neural network with the flip method for augmentation   
    x = data.reshape(-1,3,32,32).transpose(0,2,3,1)   
    #for i in range(32):
    return x[:,:,::-1,:].transpose(0,3,1,2).reshape(-1, 3*32*32)
   
def test_lenet_flip():
    test_lenet(learning_rate=0.1, n_epochs=1000, nkerns=[32, 64],
               batch_size=200, verbose=False,flip=True)
    
    
#Problem 2.4
#Write a function to add noise, it should at least provide Gaussian-distributed and uniform-distributed noise with zero mean
def noise_injection(x, var=0.01, uniform=False):
#Implement a convolutional neural network with the augmentation of injecting noise into input
   if uniform==False:    
      return x + var*np.random.randn(*x.shape)
   if uniform==True:
      return x + var*np.random.rand(*x.shape)
      
def test_lenet_inject_noise_input():
    test_lenet(learning_rate=0.1, n_epochs=1000, nkerns=[32, 64],
               batch_size=200, verbose=False,noise=True)
   
def plot_top_16(D, sz, imname):
    x = D.reshape(-1,3,32,32).transpose(0,2,3,1)
    f, axarr = plt.subplots(4, 4)
    for i in range(axarr.shape[0]):
        for j in range(axarr.shape[1]):
            axarr[i, j].imshow(x[i*4+j,:,:,:])
            axarr[i, j].set_title(i*4+j+1)
            axarr[i, j].axis('off')
    f.savefig(imname, bbox_inches='tight')
    plt.close(f)
    
    
def ImageToMatrix(image):
    data = image.getdata()
    data = np.matrix(data,dtype='float')/255.0
    new_data1 = np.reshape(data[:,0],(32,32))
    new_data2 = np.reshape(data[:,1],(32,32))
    new_data3 = np.reshape(data[:,2],(32,32))
    newdata=np.array([new_data1,new_data2,new_data3])
    newdata=newdata.reshape(3072,)
    return newdata

def MatrixToImage(data):
    x = data.reshape(3,32,32).transpose(1,2,0)
    x = x*255
    new_im = Image.fromarray(x.astype(np.uint8))
    return new_im
        
#Problem 3 
#Implement a convolutional neural network to achieve at least 80% testing accuracy on CIFAR-dataset
def MY_lenet(learning_rate=0.01, n_epochs=1000,batch_size=200, verbose=False):
    
    rng = numpy.random.RandomState(23455)
    
    
    train_set_t, valid_set_t, test_set_t = load_data(ds_rate=20, theano_shared=False)
    train_set_t[1] = np.tile(train_set_t[1], 5)
    train_set_x_u = translate_image(train_set_t[0], [ 0,-1])
    train_set_x_d = translate_image(train_set_t[0], [ 0, 1])
    train_set_x_r = translate_image(train_set_t[0], [ 1, 0])
    train_set_x_l = translate_image(train_set_t[0], [-1, 0])
    train_set_t[0] = np.vstack((train_set_t[0],
                              train_set_x_u,
                              train_set_x_d,
                              train_set_x_r,
                              train_set_x_l))

       
    train_set_r, valid_set_r, test_set_r = load_data(ds_rate=20, theano_shared=False)
    train_set_r[1] = np.tile(train_set_r[1], 5)
    train_set_x_5 = rotate_image(train_set_r[0], degree=5)
    train_set_x_10 = rotate_image(train_set_r[0], degree=10)
    train_set_x_n5 = rotate_image(train_set_r[0], degree=-5)
    train_set_x_n10 = rotate_image(train_set_r[0], degree=-10)
    train_set_r[0] = np.vstack((train_set_r[0],
                              train_set_x_5,
                              train_set_x_10,
                              train_set_x_n5,
                              train_set_x_n10))
    
       
       
    
    train_set_f, valid_set_f, test_set_f = load_data(ds_rate=8, theano_shared=False)
    train_set_f[1] = np.tile(train_set_f[1], 2)
    train_set_f[0] = np.vstack((train_set_f[0],flip_image(train_set_f[0])))
    
       
    
    
    
    #train_set_n, valid_set_n, test_set_n = load_data(ds_rate=2, theano_shared=False)
    #train_set_n=noise_injection(train_set_n[0])
    #train_set_n[0] = np.vstack((train_set_n[0],train_set_n))
    #train_set_n[1] = np.tile(train_set_n[1], 2)
       
       
       
       
        
    datasets = load_data(ds_rate=4,theano_shared=False)
    train_set_x, train_set_y = datasets[0]
    
    
    train_set_x,train_set_y = np.vstack((train_set_x,train_set_t[0],train_set_r[0],train_set_f[0])),np.concatenate((train_set_y,train_set_t[1],train_set_r[1],train_set_f[1]))
    print(train_set_y.shape)   
    print(train_set_x.shape) 
    
    train_set_x,train_set_y=shared_dataset((train_set_x,train_set_y))    
    
    datasets2 = load_data()
    valid_set_x, valid_set_y = datasets2[1]
    test_set_x, test_set_y = datasets2[2]
    
    
    
       
    # compute number of minibatches for training, validation and testing
    n_train_batches = train_set_x.get_value(borrow=True).shape[0]
    n_valid_batches = valid_set_x.get_value(borrow=True).shape[0]
    n_test_batches = test_set_x.get_value(borrow=True).shape[0]
    n_train_batches //= batch_size
    n_valid_batches //= batch_size
    n_test_batches //= batch_size
    
    
    

    # allocate symbolic variables for the data
    index = T.lscalar()  # index to a [mini]batch
   
    x = T.matrix('x')   # the data is presented as rasterized images
    y = T.ivector('y')  # the labels are presented as 1D vector of
                        # [int] labels
    training_enabled = T.iscalar('training_enabled')

    ######################
    # BUILD ACTUAL MODEL #
    ######################
    print('... building the model')

    # Reshape matrix of rasterized images of shape (batch_size, 3 * 32 * 32)
    # to a 4D tensor, compatible with our LeNetConvPoolLayer
    layer0_input = x.reshape((batch_size, 3, 32, 32))

    # Construct the first convolutional pooling layer:
    # filtering reduces the image size to (32-5+1 , 32-5+1) = (28, 28)
    # maxpooling reduces this further to (28/3, 28/3) = (9, 9)
    # 4D output tensor is thus of shape (batch_size, nkerns[0], 9, 9)
    # :type filter_shape: tuple or list of length 4
    # :param filter_shape: (number of filters, num input feature maps,
    #                              filter height, filter width)
    #
    #:type image_shape: tuple or list of length 4
    #:param image_shape: (batch size, num input feature maps,
    #   image height, image width)
    layer0 = LeNetConvPoolLayer(
        rng,
        input=layer0_input,
        image_shape=(batch_size, 3, 32, 32),
        filter_shape=(32, 3, 3, 3),
        poolsize=(2, 2)
    )

    # Construct the second convolutional pooling layer
    # filtering reduces the image size to (9-7+1, 9-7+1) = (3, 3)
    # maxpooling reduces this further to (3/2, 3/2) = (1, 1)
    # 4D output tensor is thus of shape (batch_size, nkerns[1], 1, 1)
    layer1 = LeNetConvPoolLayer(
        rng,
        input=layer0.output,
        image_shape=(batch_size, 32, 15, 15),
        filter_shape=(64, 32, 3, 3),
        poolsize=(2, 2)
    )
    


    # the HiddenLayer being fully-connected, it operates on 2D matrices of
    # shape (batch_size, num_pixels) (i.e matrix of rasterized images).
    # This will generate a matrix of shape (batch_size, nkerns[1] * 2 * 2),
    # or (500, 50 * 4 * 4) = (500, 800) with the default values.
    layer5_input = layer1.output.flatten(2)

    # construct a fully-connected sigmoidal layer
    layer5 = HiddenLayer(
        rng,
        input=layer5_input,
        n_in=2304,
        n_out=4096,
        activation=T.tanh,
        FC_layer=True
    )
    
    layer6 = DropoutHiddenLayer(
        rng,
        input=layer5.output,
        is_train=training_enabled,
        n_in=4096,
        n_out=4096,
        activation=T.tanh
    )
    
    layer7 = HiddenLayer(
        rng,
        input=layer6.output,
        n_in=4096,
        n_out=512,
        activation=T.tanh,
    )
    
#    layer8 = DropoutHiddenLayer(
#        rng,
#        input=layer7.output,
#        is_train=training_enabled,
#        n_in=800,
#        n_out=800,
#        activation=T.tanh
#    )

    # classify the values of the fully-connected sigmoidal layer
    layer9 = LogisticRegression(input=layer7.output, n_in=512, n_out=10)

    # the cost we minimize during training is the NLL of the model
    cost = layer9.negative_log_likelihood(y)

    # create a function to compute the mistakes that are made by the model
    test_model = theano.function(
        [index],
        layer9.errors(y),
        givens={
            x: test_set_x[index * batch_size: (index + 1) * batch_size],
            y: test_set_y[index * batch_size: (index + 1) * batch_size],
            training_enabled: numpy.cast['int32'](0)
        }
    )

    validate_model = theano.function(
        [index],
        layer9.errors(y),
        givens={
            x: valid_set_x[index * batch_size: (index + 1) * batch_size],
            y: valid_set_y[index * batch_size: (index + 1) * batch_size],
            training_enabled: numpy.cast['int32'](0)
        }
    )

    # create a list of all model parameters to be fit by gradient descent
    params =layer9.params+layer7.params+layer6.params+layer5.params+ layer1.params + layer0.params

    # create a list of gradients for all model parameters
    grads = T.grad(cost, params)

    # train_model is a function that updates the model parameters by
    # SGD Since this model has many parameters, it would be tedious to
    # manually create an update rule for each model parameter. We thus
    # create the updates list by automatically looping over all
    # (params[i], grads[i]) pairs.
    updates = OrderedDict()
    one = T.constant(1)
    rho=T.constant(0.9)
    epsilon=T.constant(1e-6)
    for param, grad in zip(params, grads):
        value = param.get_value(borrow=True)
        accu = theano.shared(np.zeros(value.shape, dtype=value.dtype),
                             broadcastable=param.broadcastable)
        accu_new = rho * accu + (one - rho) * grad ** 2
        updates[accu] = accu_new
        updates[param] = param - (learning_rate * grad /
                                      T.sqrt(accu_new + epsilon))
        

    train_model = theano.function(
        [index],
        cost,
        updates=updates,
        givens={
            x: train_set_x[index * batch_size: (index + 1) * batch_size],
            y: train_set_y[index * batch_size: (index + 1) * batch_size],
            training_enabled: numpy.cast['int32'](1)
        }
    )

    ###############
    # TRAIN MODEL #
    ###############
    print('... training')

    train_nn(train_model, validate_model, test_model,
             n_train_batches, n_valid_batches, n_test_batches,
             n_epochs, verbose)

def drop(input, p=0.7): 
    """
    :type input: numpy.array
    :param input: layer or weight matrix on which dropout is applied
    
    :type p: float or double between 0. and 1. 
    :param p: p probability of NOT dropping out a unit, therefore (1.-p) is the drop rate.
    
    """            
    rng = numpy.random.RandomState(1234)
    srng = T.shared_randomstreams.RandomStreams(rng.randint(999999))
    mask = srng.binomial(n=1, p=p, size=input.shape, dtype=theano.config.floatX)
    return input * mask
    
class DropoutHiddenLayer(object):
    def __init__(self, rng, is_train, input, n_in, n_out, W=None, b=None,
                 activation=T.tanh, p=0.7):
        """
        Hidden unit activation is given by: activation(dot(input,W) + b)

        :type rng: numpy.random.RandomState
        :param rng: a random number generator used to initialize weights
        
        :type is_train: theano.iscalar   
        :param is_train: indicator pseudo-boolean (int) for switching between training and prediction

        :type input: theano.tensor.dmatrix
        :param input: a symbolic tensor of shape (n_examples, n_in)

        :type n_in: int
        :param n_in: dimensionality of input

        :type n_out: int
        :param n_out: number of hidden units

        :type activation: theano.Op or function
        :param activation: Non linearity to be applied in the hidden
                           layer
                           
        :type p: float or double
        :param p: probability of NOT dropping out a unit   
        """
        self.input = input

        if W is None:
            W_values = numpy.asarray(
                rng.uniform(
                    low=-numpy.sqrt(6. / (n_in + n_out)),
                    high=numpy.sqrt(6. / (n_in + n_out)),
                    size=(n_in, n_out)
                ),
                dtype=theano.config.floatX
            )
            if activation == theano.tensor.nnet.sigmoid:
                W_values *= 4

            W = theano.shared(value=W_values, name='W', borrow=True)

        if b is None:
            b_values = numpy.zeros((n_out,), dtype=theano.config.floatX)
            b = theano.shared(value=b_values, name='b', borrow=True)

        
        self.W = W
        self.b = b

        lin_output = T.dot(input, self.W) + self.b
        
        output = activation(lin_output)
        
        # multiply output and drop -> in an approximation the scaling effects cancel out 
        train_output = drop(output,p)
        
        #is_train is a pseudo boolean theano variable for switching between training and prediction 
        self.output = T.switch(T.neq(is_train, 0), train_output, p*output)
        
        # parameters of the model
        self.params = [self.W, self.b]
#Problem4
#Implement the convolutional neural network depicted in problem4 
def MY_CNN(learning_rate=0.01, n_epochs=1000, batch_size=200,p=0.7,
           momentum=False,rmsprop=False,adam=False,verbose=False):
    
    rng = numpy.random.RandomState(23455)
       
       
    train_set, valid_set, test_set = load_data( theano_shared=False)
    t=train_set[0][range(200),:]

       
    test_set_x, test_set_y = shared_dataset(test_set)
    valid_set_x, valid_set_y = shared_dataset(valid_set)
    train_set_x, train_set_y = shared_dataset(train_set)
       
       
    

    # compute number of minibatches for training, validation and testing
    n_train_batches = train_set_x.get_value(borrow=True).shape[0]
    n_valid_batches = valid_set_x.get_value(borrow=True).shape[0]
    n_test_batches = test_set_x.get_value(borrow=True).shape[0]
    n_train_batches //= batch_size
    n_valid_batches //= batch_size
    n_test_batches //= batch_size
    
    
    

    # allocate symbolic variables for the data
    index = T.lscalar()  # index to a [mini]batch

    x = T.matrix('x')   # the data is presented as rasterized images
    #y = T.ivector('y')  # the labels are presented as 1D vector of
                        # [int] labels

    ######################
    # BUILD ACTUAL MODEL #
    ######################
    print('... building the model')

    # Reshape matrix of rasterized images of shape (batch_size, 3 * 32 * 32)
    # to a 4D tensor, compatible with our LeNetConvPoolLayer
    layer0_input = x.reshape((batch_size, 3, 32, 32))
    layer0_output=drop(layer0_input,p)

    # Construct the first convolutional pooling layer:
    # filtering reduces the image size to (32-5+1 , 32-5+1) = (28, 28)
    # maxpooling reduces this further to (28/3, 28/3) = (9, 9)
    # 4D output tensor is thus of shape (batch_size, nkerns[0], 9, 9)
    # :type filter_shape: tuple or list of length 4
    # :param filter_shape: (number of filters, num input feature maps,
    #                              filter height, filter width)
    #
    #:type image_shape: tuple or list of length 4
    #:param image_shape: (batch size, num input feature maps,
    #   image height, image width)


    #image_shape=[mini-batch size, number of input feature maps,
    # image height, image width]
    #filter shape=[number of feature maps at layer m, 
    #number of feature maps at layer m-1, filter height, filter width]
    layer1 = LeNetConvPoolLayer(
        rng,
        input=layer0_output,
        image_shape=(batch_size, 3, 32, 32),
        filter_shape=(64, 3, 3, 3),
        border_mode='half',
        poolsize=(1,1),
        nopooling=True
    )

    # Construct the second convolutional pooling layer
    # filtering reduces the image size to (9-7+1, 9-7+1) = (3, 3)
    # maxpooling reduces this further to (3/2, 3/2) = (1, 1)
    # 4D output tensor is thus of shape (batch_size, nkerns[1], 1, 1)
    layer2_1 = LeNetConvPoolLayer(
        rng,
        input=layer1.output,
        image_shape=(batch_size, 64, 32, 32),
        filter_shape=(64, 64, 3, 3),
        border_mode='half',
        poolsize=(1,1),
        nopooling=True
    )
    
    
    layer2_2_output_ds = pool.pool_2d(
            input=layer2_1.output,
            ds=(2,2),
            ignore_border=False
    )
            
    
    
    layer3 = LeNetConvPoolLayer(
        rng,
        input=layer2_2_output_ds,
        image_shape=(batch_size, 64, 16, 16),
        filter_shape=(128, 64, 3, 3),
        border_mode='half',
        poolsize=(1,1),
        nopooling=True
    )
    
    
    layer4_1 = LeNetConvPoolLayer(
        rng,
        input=layer3.output,
        image_shape=(batch_size, 128, 16, 16),
        filter_shape=(128, 128, 3, 3),
        border_mode='half',
        poolsize=(1,1)
    )
    
    
    layer4_2_output_ds = pool.pool_2d(
            input=layer4_1.output,
            ds=(2,2),
            ignore_border=False
    )
    
    
    layer5 = LeNetConvPoolLayer(
        rng,
        input=layer4_2_output_ds,
        image_shape=(batch_size, 128,8,8),
        filter_shape=(256, 128, 3, 3),
        border_mode='half',
        poolsize=(2, 2),
        upsampling=True
    )
    
    # The order of upsampling and conv
    layer6 = LeNetConvPoolLayer(
        rng,
        input=layer5.output,
        image_shape=(batch_size, 256, 16,16),
        filter_shape=(128, 256, 3, 3),
        border_mode='half',
        poolsize=(1,1),
        nopooling=True
    )
    
    
    layer7 = LeNetConvPoolLayer(
        rng,
        input=layer6.output,
        image_shape=(batch_size, 128, 16, 16),
        filter_shape=(128,128, 3, 3),
        border_mode='half',
        poolsize=(1,1),
        nopooling=True
    )
    
    #upsampling
    layer8_output = theano.tensor.nnet.abstract_conv.bilinear_upsampling(input=layer7.output+layer4_1.output,ratio=2)
    
    
    layer9 = LeNetConvPoolLayer(
        rng,
        input=layer8_output,
        image_shape=(batch_size, 128, 32, 32),
        filter_shape=(64, 128, 3, 3),
        border_mode='half',
        poolsize=(1,1),
        nopooling=True
    )
    
    
    layer10 = LeNetConvPoolLayer(
        rng,
        input=layer9.output,
        image_shape=(batch_size, 64, 32, 32),
        filter_shape=(64,64, 3, 3),
        border_mode='half',
        poolsize=(1,1),
        nopooling=True
    )
    
    
    layer11_output = layer10.output+layer2_1.output
    
    
    layer12 = LeNetConvPoolLayer(
        rng,
        input=layer11_output,
        image_shape=(batch_size, 64, 32, 32),
        filter_shape=(3, 64, 3, 3),
        border_mode='half',
        poolsize=(1,1)
    )
    

    # the cost we minimize during training is the NLL of the model
    cost = T.mean((layer0_input-layer12.output)**2)

    # create a function to compute the mistakes that are made by the model
    test_model = theano.function(
        [index],
        cost,
        givens={
            x: test_set_x[index * batch_size: (index + 1) * batch_size]
        }
    )

    validate_model = theano.function(
        [index],
        cost,
        givens={
            x: valid_set_x[index * batch_size: (index + 1) * batch_size]
        }
    )

    # create a list of all model parameters to be fit by gradient descent
    params = layer12.params +layer10.params +layer9.params +layer7.params +layer6.params +layer5.params+layer4_1.params + layer3.params + layer2_1.params + layer1.params

    # create a list of gradients for all model parameters
    grads = T.grad(cost, params)

    # train_model is a function that updates the model parameters by
    # SGD Since this model has many parameters, it would be tedious to
    # manually create an update rule for each model parameter. We thus
    # create the updates list by automatically looping over all
    # (params[i], grads[i]) pairs.
    if momentum==True:
        momentum =theano.shared(numpy.cast[theano.config.floatX](0.5), name='momentum')
        updates = []
        for param in  params:
            param_update = theano.shared(param.get_value()*numpy.cast[theano.config.floatX](0.))    
            updates.append((param, param - learning_rate*param_update))
            updates.append((param_update, momentum*param_update + (numpy.cast[theano.config.floatX](1.) - momentum)*T.grad(cost, param)))
    
    
    if rmsprop==True:
        updates = OrderedDict()
        one = T.constant(1)
        rho=T.constant(0.9)
        epsilon=T.constant(1e-6)
        for param, grad in zip(params, grads):
            value = param.get_value(borrow=True)
            accu = theano.shared(np.zeros(value.shape, dtype=value.dtype),
                             broadcastable=param.broadcastable)
            accu_new = rho * accu + (one - rho) * grad ** 2
            updates[accu] = accu_new
            updates[param] = param - (learning_rate * grad /
                                      T.sqrt(accu_new + epsilon))
        
        
    if adam==True:
        
        t_prev = theano.shared(numpy.cast[theano.config.floatX](0.5), name='t_prev')
        updates = OrderedDict()

        beta1=T.constant(0.9)
        beta2=T.constant(0.999)
        epsilon=T.constant(1e-8)
        one = T.constant(1)

        t = t_prev + 1
        a_t = learning_rate*T.sqrt(one-beta2**t)/(one-beta1**t)

        for param, g_t in zip(params, grads):
            value = param.get_value(borrow=True)
            m_prev = theano.shared(np.zeros(value.shape, dtype=value.dtype),
                               broadcastable=param.broadcastable)
            v_prev = theano.shared(np.zeros(value.shape, dtype=value.dtype),
                               broadcastable=param.broadcastable)

            m_t = beta1*m_prev + (one-beta1)*g_t
            v_t = beta2*v_prev + (one-beta2)*g_t**2
            step = a_t*m_t/(T.sqrt(v_t) + epsilon)

            updates[m_prev] = m_t
            updates[v_prev] = v_t
            updates[param] = param - step

        updates[t_prev] = t

    train_model = theano.function(
        [index],
        cost,
        updates=updates,
        givens={
            x: train_set_x[index * batch_size: (index + 1) * batch_size]
        }
    )

    ###############
    # TRAIN MODEL #
    ###############
    print('... training')

    train_nn(train_model, validate_model, test_model,
             n_train_batches, n_valid_batches, n_test_batches,
             n_epochs, verbose=verbose)
     
    
    truth_fcn=theano.function(
        [index],
        layer0_input,
        givens={
            x: train_set_x[index * batch_size: (index + 1) * batch_size]
        }
        )
    
    
    
    corrupted_fcn=theano.function(
        [index],
        layer0_output,
        givens={
            x: train_set_x[index * batch_size: (index + 1) * batch_size]
        }
        )
    
    
    restored_fcn=theano.function(
        [index],
        layer12.output,
        givens={
            x: train_set_x[index * batch_size: (index + 1) * batch_size]
        }
        )
    train_set_x=t    
    truth=truth_fcn(0)
    corrupted=corrupted_fcn(0)
    restored=restored_fcn(0)
    
    plot_8x3(truth,corrupted,restored,32,'plot_stored')
    
    
def plot_8x3(truth,corrupted,restored, sz, imname):
    truth = truth.reshape(-1,3,32,32).transpose(0,2,3,1)
    corrupted = corrupted.reshape(-1,3,32,32).transpose(0,2,3,1)
    restored = restored.reshape(-1,3,32,32).transpose(0,2,3,1)
    
    f, axarr = plt.subplots(8, 3)
    for i in range(axarr.shape[0]):       
            axarr[i, 0].imshow(truth[i,:,:,:])
            axarr[i, 0].set_title('ground truth')
            axarr[i, 0].axis('off')
            axarr[i, 1].imshow(corrupted[i,:,:,:])
            axarr[i, 1].set_title('corrupted input')
            axarr[i, 1].axis('off')
            axarr[i, 2].imshow(restored[i,:,:,:])
            axarr[i, 2].set_title('restored output')
            axarr[i, 2].axis('off')
    f.savefig(imname, bbox_inches='tight')
    plt.close(f)

if __name__ == '__main__':
#    test_lenet(verbose=True)
#    test_lenet_translation()
#    test_lenet_rotation()
#    test_lenet_flip()
#    test_lenet_inject_noise_input()
#    MY_lenet(learning_rate=0.01, n_epochs=1000,batch_size=200, verbose=True)

#    MY_CNN(learning_rate=0.01, n_epochs=1000, batch_size=200,p=0.7,
#           rmsprop=True,adam=False,verbose=True)
    MY_CNN(learning_rate=0.01, n_epochs=1, batch_size=200,p=0.7,
          adam=True,verbose=True)
